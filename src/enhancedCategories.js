const toArray = (categories) =>
  Object.keys(categories).map((key) => ({ key, label: categories[key] }));

const toEnhancedCategories = ({
  categories,
  deletedCategories = [],
  addedCategories = [],
}) => {
  const initialManagedCategories = toArray(categories).map(
    ({ key, label }) => ({
      key,
      label,
      newLabel: label,
      original: true,
      deleted: deletedCategories.map(String).includes(key),
    })
  );

  return addedCategories.reduce((managedCategories, addedCategory) => {
    const index = managedCategories.findIndex(
      (managedCategory) => managedCategory.key === String(addedCategory.key)
    );
    if (index !== -1) {
      managedCategories[index] = {
        ...managedCategories[index],
        newLabel: addedCategory.label,
      };
    } else {
      managedCategories.push({
        ...addedCategory,
        key: String(addedCategory.key),
        newLabel: addedCategory.label,
        original: false,
        deleted: deletedCategories.map(String).includes(managedCategories.key),
      });
    }

    return managedCategories;
  }, initialManagedCategories);
};

module.exports = {
  toEnhancedCategories,
};
