const http = require('http');
const https = require('https');
const { URL } = require('url');

const fetchCategories = async ({ config, logger }) => {
  const categoriesEndpoint = `${config.getWebserverUrl()}/api/v1/videos/categories`;
  logger.debug(`fetching categories from ${categoriesEndpoint}`);

  return await get(categoriesEndpoint);
};

function get(endpoint) {
  const endpointURL = new URL(endpoint);
  const adapter = endpointURL.protocol === 'https:' ? https : http;

  return new Promise((resolve, reject) =>
    adapter
      .get(endpoint, (res) => {
        let data = [];
        res.on('data', (chunk) => {
          data.push(chunk);
        });

        res.on('end', () => {
          console.log('Response ended: ');
          try {
            const json = JSON.parse(Buffer.concat(data).toString());
            resolve(json);
          } catch (e) {
            reject(e);
          }
        });
      })
      .on('error', (err) => {
        reject(err);
      })
  );
}

module.exports = {
  fetchCategories,
};
