const classNames = (classes = []) => classes.filter((c) => !!c).join(' ');

const renderEnhancedCategoryTableRow = (category = {}) => {
  return `<tr class="${classNames([
    'category',
    category.deleted && 'category__deleted',
    !category.original && 'category__added',
  ])}">
    <td>${category.key}</td>
    <td>${
      category.label !== category.newLabel
        ? `<s>${category.label}</s> ${category.newLabel}`
        : category.label
    }</td>
</tr>`;
};

const renderEnhancedCategoriesTable = (enhancedCategories) => {
  return `<table id="categories-table">
    <thead>
      <tr>
        <th>key</th>
        <th>label</th>
      </tr>
    </thead>
    <tbody>
      ${enhancedCategories.map(renderEnhancedCategoryTableRow).join('')}
    </tbody>
  </table>`;
};

module.exports = {
  renderEnhancedCategoriesTable,
};
