const { fetchCategories } = require('./api');
const customCategoriesInitialState = { add: [], delete: [] };

/**
 * Fetch instance categories from the PeerTube API and store them.
 */
const storeInitialCategories = async ({ config, storageManager, logger }) => {
  const categories = await fetchCategories({ config, logger });
  logger.debug('storing initial categories', categories);

  await storageManager.storeData('initialCategories', categories);

  return categories;
};

const getCustomCategories = async ({ settingsManager }) => {
  const customCategoriesAsText = await settingsManager.getSetting(
    'json-categories-as-text'
  );
  try {
    return JSON.parse(customCategoriesAsText);
  } catch (e) {
    console.error(e);
    console.error(customCategoriesAsText);
  }

  return customCategoriesInitialState;
};

const handleChanges =
  ({ videoCategoryManager, storageManager, config, logger }) =>
  async (settings) => {
    try {
      logger.debug('running settings change handler');
      // add each initial categories in case they were previously deleted
      const initialCategories = await storageManager.getData(
        'initialCategories'
      );
      Object.keys(initialCategories).forEach((key) =>
        videoCategoryManager.addCategory(key, initialCategories[key])
      );

      // remove categories which weren't presents in the initial categories
      const categories = await fetchCategories({ config, logger });
      Object.keys(categories)
        .filter((key) => !initialCategories.hasOwnProperty(key))
        .forEach((key) => videoCategoryManager.deleteCategory(key));

      // @todo an original category label may have changed.
      // detect this change, delete and add again with initial label.

      // read settings and update categories
      const customCategories = JSON.parse(settings['json-categories-as-text']);
      logger.debug('parsed custom categories', customCategories);
      if (customCategories.hasOwnProperty('add')) {
        const categoriesToAdd = customCategories['add'];
        categoriesToAdd.forEach(({ key, label }) =>
          videoCategoryManager.addCategory(key, label)
        );
      }

      if (customCategories.hasOwnProperty('delete')) {
        const categoriesToDelete = customCategories['delete'];
        categoriesToDelete.forEach((key) =>
          videoCategoryManager.deleteCategory(key)
        );
      }
    } catch (e) {
      logger.log({ level: 'error', message: e });
    }
  };

module.exports = {
  customCategoriesInitialState,
  storeInitialCategories,
  getCustomCategories,
  handleChanges,
};
