const fetchEnhancedCategories = () => {
  const headers = new Headers();
  headers.append('Accept', 'text/html');
  return fetch('/plugins/categories/router/enhanced-categories', {
    headers,
  }).then((response) => response.text());
};

const someNodeHasClassName = (nodeList, className) =>
  nodeList &&
  Array.from(nodeList).some((node) => {
    if (node.classList.contains(className)) {
      return true;
    }

    return someNodeHasClassName(node.childNodes, className);
  });

/**
 * Detect success event by looking for a success toaster on the page.
 */
const addUpdateSuccessListener = (successCallback = () => {}) => {
  const toastClassName = 'p-toast-bottom-right';
  const toastNode = document.querySelector(`.${toastClassName}`);
  if (toastNode) {
    const observer = new MutationObserver(
      // update the enhanced categories table if we found the success toast
      (mutationsList) => {
        for (const mutation of mutationsList) {
          if (
            mutation.type === 'childList' &&
            someNodeHasClassName(mutation.addedNodes, 'p-toast-message-success')
          ) {
            successCallback();
          }
        }
      }
    );
    observer.observe(toastNode, { attributes: false, childList: true });
  } else {
    console.error(`Could not find element with class name: ${toastClassName}`);
  }
};

const updateCategoriesTable = (categoriesTable) => {
  const categoriesTableElement = document.querySelector(
    '#categoriesTable-wrapper table'
  );
  if (categoriesTableElement) {
    categoriesTableElement.parentElement.innerHTML = categoriesTable;
  }
};

function register({ registerHook }) {
  registerHook({
    target: 'action:admin-plugin-settings.init',
    handler: ({ npmName }) => {
      if ('peertube-plugin-categories' !== npmName) {
        return;
      }

      fetchEnhancedCategories().then(updateCategoriesTable);

      document
        .querySelector('#json-categories-as-text')
        .addEventListener('input', ({ target }) => {
          try {
            JSON.parse(target.value);
            target.classList.remove('error');
          } catch (e) {
            target.classList.add('error');
          }
        });

      addUpdateSuccessListener(() =>
        fetchEnhancedCategories().then(updateCategoriesTable)
      );
    },
  });
}

export { register };
